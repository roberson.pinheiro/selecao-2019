<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


//Api publico
Route::group(['prefix' => 'api'], function(){
	//Pega sessao do usuario
	Route::get('/session',[
    		'uses' => 'PessoaController@token',
			'as' => 'pessoa.session'
		]
	);
	
	//Rota de Pessoas
	Route::get('/pessoas',[
    		'uses' => 'PessoaController@list',
			'as' => 'pessoa.pessoas'
		]
	);

	Route::post('/pessoa/store',[
    		'uses' => 'PessoaController@store',
			'as' => 'pessoa.store'
		]
	);

	Route::get('/pessoa/view/{id}',[
    		'uses' => 'PessoaController@view',
			'as' => 'pessoa.view'
		]
	);

	Route::put('/pessoa/update/{id}',[
    		'uses' => 'PessoaController@update',
			'as' => 'pessoa.update'
		]
	);

	Route::delete('/pessoa/destroy/{id}',[
    		'uses' => 'PessoaController@destroy',
			'as' => 'pessoa.destroy'
		]
	);

	//Rota de endereços
	Route::get('/endereco/view/{pessoasId}',[
    		'uses' => 'EnderecoController@view',
			'as' => 'endereco.view'
		]
	);

	Route::post('/endereco/store/{pessoasId}',[
    		'uses' => 'EnderecoController@store',
			'as' => 'endereco.store'
		]
	);

	Route::put('/endereco/update/{id}',[
    		'uses' => 'EnderecoController@update',
			'as' => 'endereco.update'
		]
	);

	Route::delete('/endereco/destroy/{id}',[
    		'uses' => 'EnderecoController@destroy',
			'as' => 'endereco.destroy'
		]
	);	

});