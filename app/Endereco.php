<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Endereco extends Model
{
	public $timestamps = false;
	protected $table = 'endereco';

	protected $fillable = [
		'cep',
		'endereco',
		'cidade',
		'estado',
		'numero',
		'complemento',
		'titulo',
		'pessoa_id',
    ];

    public $estadosBrasileiros = array(
		'AC'=>'Acre',
		'AL'=>'Alagoas',
		'AP'=>'Amapá',
		'AM'=>'Amazonas',
		'BA'=>'Bahia',
		'CE'=>'Ceará',
		'DF'=>'Distrito Federal',
		'ES'=>'Espírito Santo',
		'GO'=>'Goiás',
		'MA'=>'Maranhão',
		'MT'=>'Mato Grosso',
		'MS'=>'Mato Grosso do Sul',
		'MG'=>'Minas Gerais',
		'PA'=>'Pará',
		'PB'=>'Paraíba',
		'PR'=>'Paraná',
		'PE'=>'Pernambuco',
		'PI'=>'Piauí',
		'RJ'=>'Rio de Janeiro',
		'RN'=>'Rio Grande do Norte',
		'RS'=>'Rio Grande do Sul',
		'RO'=>'Rondônia',
		'RR'=>'Roraima',
		'SC'=>'Santa Catarina',
		'SP'=>'São Paulo',
		'SE'=>'Sergipe',
		'TO'=>'Tocantins'
	);
}