<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class EstadoServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('estado', function($attribute, $value, $parameters, $validator) {
            $dados = $this->app->request->all();
            $estadosBrasileiros = new \App\Endereco;

            //var_dump($dados['estado'], );exit;

            if(array_key_exists($dados['estado'], $estadosBrasileiros->estadosBrasileiros)){
                return true;
            }
            
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
