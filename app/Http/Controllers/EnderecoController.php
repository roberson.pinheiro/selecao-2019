<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Session;
use App\Endereco;
use App\Pessoa;

class EnderecoController extends Controller
{

    protected function validationRules($id)
    {
        $required = empty($id) ? 'required' : 'nullable';

        return $validationRules = [
            'cep' => $required.'|string|max:9',
            'endereco' => $required.'|string',
            'cidade' => $required.'|string|max:100',
            'estado' => $required.'|string|max:2|estado',
            'numero' => $required.'|string|max:20',
            'complemento' => 'nullable|string|max:255',
            'titulo' => $required.'|string|max:100',
        ];
    }

    public function getRetorno($status, $error, $itens){
        $retorno = array();

        $retorno = [
            'status' => $status,
            'error' => $error,
            'itens'  => $itens,
        ];

        return $retorno;
    }

    public function validateJson($request, $id){
        $validator = Validator::make($request, $this->validationRules($id));

        if($validator->fails()){
            return $this->getRetorno(false, $validator->messages(), null);
        }
    }

    public function store(Request $request, $id)
    {
        $validate = $this->validateJson($request->all(), 0);

        //Verifica se pessoa existe
        $pessoa = Pessoa::find($id);

        if(!$pessoa){
            return $this->getRetorno('false', 'Pessoa informada não encontrada!', $pessoa);
        }

        if($validate){
            return $validate;
        }

        $dados = $request->all();
        $dados['pessoa_id'] = $id;

        $endereco = Endereco::create($dados);

        if($endereco){
            return $this->getRetorno('true', null, $endereco);
        }else{
            return $this->getRetorno('false', 'Erro ao salvar Endereço', null);
        }
    }

    
    public function view($id)
    {
        $endereco = Endereco::find($id);

        if($endereco){
            return $this->getRetorno('true', null, $endereco);
        }elseif($endereco === NULL){
            return $this->getRetorno('true', null, $endereco);
        }else{
            return $this->getRetorno('false', 'Erro ao salvar endereço', null);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {        
        $endereco = Endereco::find($id);

        if(!$endereco){
            return $this->getRetorno('false', 'Nenhum endereço encontrado!', $endereco);
        }
        
        if($request->cep){
            $endereco->cep = $request->cep;    
        }

        if($request->endereco){
            $endereco->endereco = $request->endereco;    
        }

        if($request->cidade){
            $endereco->cidade = $request->cidade;    
        }
        
        if($request->estado){
            $endereco->estado = $request->estado;    
        }

        if($request->numero){
            $endereco->numero = $request->numero;    
        }

        if($request->complemento){
            $endereco->complemento = $request->complemento;    
        }

        if($request->titulo){
            $endereco->titulo = $request->titulo;    
        }

        if($request->pessoa_id){
            $endereco->pessoa_id = $request->pessoa_id;    
        }

        //$this->validate($pessoa, $this->validationRules);
        $validate = $this->validateJson($request->all(), $id);

        if($validate){
            return $validate;
        }

        $retorno = $endereco->save();

        if($retorno){
            return $this->getRetorno('true', null, $endereco);
        }else{
            return $this->getRetorno('false', 'Erro ao salvar pessoa', null);
        }
        
    }

    public function destroy($id)
    {
        $endereco = Endereco::find($id);

        if($endereco){
            if($endereco->delete()){
                return $this->getRetorno('true', null, $endereco);
            }else{
                return $this->getRetorno('false', 'Erro ao deletar endereço!', $endereco);
            }
        }else{
            return $this->getRetorno('false', 'Nenhum endereço encontrado!', $endereco);
        }
    }
}