<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Session;
use App\Pessoa;
use App\Endereco;

class PessoaController extends Controller
{

    protected function validationRules($id)
    {
        $required = empty($id) ? 'required' : 'nullable';

        return $validationRules = [
            'nome' => $required.'|string|max:255',
            'email' => $required.'|string|email|max:255|unique:pessoa',
        ];
    }

    public function getRetorno($status, $error, $itens){
        $retorno = array();

        $retorno = [
            'status' => $status,
            'error' => $error,
            'itens'  => $itens,
        ];

        return $retorno;
    }

    public function validateJson($request, $id){
        $validator = Validator::make($request, $this->validationRules($id));

        if($validator->fails()){
            return $this->getRetorno(false, $validator->messages(), null);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list()
    {
        $pessoa = new Pessoa();

        $retorno = $pessoa->getPessoas();

        if($retorno){
            return $this->getRetorno('true', null, $retorno);
        }else{
            return $this->getRetorno('false', 'Erro ao listar pessoas!', null);
        }
    }

    public function token()
    {
        $token = [
            'token' => csrf_token()
        ];

        return $this->getRetorno('true', null, $token);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */    
    public function store(Request $request)
    {
        $validate = $this->validateJson($request->all(), 0);

        if($validate){
            return $validate;
        }

        $dados = $request->all();
        $pessoa = Pessoa::create($dados);

        if($pessoa){
            return $this->getRetorno('true', null, $pessoa);
        }else{
            return $this->getRetorno('false', 'Erro ao salvar pessoa', null);
        }
    }
    
    public function view($id)
    {
        $pessoa = Pessoa::find($id);

        $pessoa->enderecos = Endereco::where('pessoa_id', $id)->get();

        if($pessoa){
            return $this->getRetorno('true', null, $pessoa);
        }elseif($pessoa === NULL){
            return $this->getRetorno('true', null, $pessoa);
        }else{
            return $this->getRetorno('false', 'Erro ao salvar pessoa', null);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {        
        $pessoa = \App\Pessoa::find($id);

        if(!$pessoa){
            return $this->getRetorno('false', 'Nenhuma pessoa encontrada!', $pessoa);
        }
        
        if($request->nome){
            $pessoa->nome = $request->nome;    
        }
        
        if($request->email){
            $pessoa->email = $request->email;    
        }

        if($request->sexo){
            $pessoa->sexo = $request->sexo;    
        }

        if($request->data_nascimento){
            $pessoa->data_nascimento = $request->data_nascimento;    
        }

        if($request->telefone){
            $pessoa->telefone = $request->telefone;    
        }

        if($request->telefone_celular){
            $pessoa->telefone_celular = $request->telefone_celular;    
        }

        //$this->validate($pessoa, $this->validationRules);
        $validate = $this->validateJson($request->all(), $id);

        if($validate){
            return $validate;
        }

        $retorno = $pessoa->save();

        if($retorno){
            return $this->getRetorno('true', null, $pessoa);
        }else{
            return $this->getRetorno('false', 'Erro ao salvar pessoa', null);
        }
        
    }

    public function destroy($id)
    {
        $pessoa = Pessoa::find($id);
        $endereco = Endereco::where('pessoa_id', $id);

        if($pessoa){
            //Deletar Endereços ligados ao usuários

            if($pessoa->delete() && $endereco->delete()){
                return $this->getRetorno('true', null, $pessoa);
            }else{
                return $this->getRetorno('false', 'Erro ao deletar pessoa!', $pessoa);
            }
        }else{
            return $this->getRetorno('false', 'Nenhuma pessoa encontrada!', $pessoa);
        }
    }
}