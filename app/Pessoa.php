<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pessoa extends Model
{
	public $timestamps = false;
	protected $table = 'pessoa';

	protected $fillable = [
		'nome',
		'data_nascimento',
		'sexo',
		'email',
		'telefone',
		'telefone_celular',
	];

	public function getPessoas()
	{
		$usuario = \App\Pessoa::orderBy('nome');
		return $usuario->get();
	}
}