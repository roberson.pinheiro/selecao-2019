
# Informações Básicas
Framework: Laravel 5.7 ([https://laravel.com/docs/5.7](https://laravel.com/docs/5.7))
Banco: Sqlite [https://www.sqlite.org](https://www.sqlite.org)
Teste Unitário: Laravel Dusk ([https://laravel.com/docs/5.7/dusk](https://laravel.com/docs/5.7/dusk))

O projeto foi desenvolvido em Laravel 5.7, e tem os requisitos abaixo do PHP conforme documentação da framework no site oficial ([https://laravel.com/docs/5.7](https://laravel.com/docs/5.7)):
- PHP >= 7.1.3
- OpenSSL PHP Extension
- PDO PHP Extension
- Mbstring PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension
- Ctype PHP Extension
- JSON PHP Extension
- BCMath PHP Extension

# Passos de instalação

*Obs: Para rodar os comandos abaixo é necessário estar na pasta raiz do projeto.*

**1º Após clonar o projeto é necessário instalar as dependências do projeto via composer:**
> composer install

**2º Renomeie o arquivo .env.example para .env**

**3º Será necessário gerar a chave da aplicação com o comando abaixo**
> php artisan key:generate

**4º Após os procedimentos acima o servidor poderá ser iniciado com o comando:**
> php artisan serve 

*Atenção: Em caso de erro na instalação das dependências do projeto considere apagar o arquivo composer.lock*

# Rotas

## Sessão
***Método:*** `GET`
[http://127.0.0.1:8000/api/session](http://127.0.0.1:8000/api/session)

Obs: Importante para o token que deverá ser utilizados em todas requisições `POST, DELETE E PUT`

## Listar pessoas
**Método:** `GET`
[http://127.0.0.1:8000/api/pessoas](http://127.0.0.1:8000/api/pessoas)

## Inserir pessoa
**Método:** `POST`
[http://127.0.0.1:8000/api/pessoa/store](http://127.0.0.1:8000/api/pessoa/store)

*Parâmetros*
- Nome (string | obrigatório)
- data_nascimento (string | obrigatório)
- sexo (integer | obrigatório) – 1 Masculino; 2 Feminino
- email (string | obrigatório)
- telefone (string | obrigatório)
- telefone_celular (string | obrigatório)
- _token (string | obrigatório)

## Visualizar pessoa
**Método:** `GET`
[http://127.0.0.1:8000/api/pessoa/view/{id}](http://127.0.0.1:8000/api/pessoa/view/%7bid%7d)

## Atualizar pessoa
**Método:** `PUT`
[http://127.0.0.1:8000/api/pessoa/update/{id}](http://127.0.0.1:8000/api/pessoa/update/%7bid%7d)

*Parâmetros*
- nome (string | não obrigatório)
- data_nascimento (string | não obrigatório)
- sexo (integer | não obrigatório) – 1 Masculino; 2 Feminino
- email (string | não obrigatório)
- telefone (string | não obrigatório)
- telefone_celular (string | obrigatório)
- _token (string | não obrigatório)

## Deletar pessoa
**Método:** `DELETE`
[http://127.0.0.1:8000/api/pessoa/destroy/{id}](http://127.0.0.1:8000/api/pessoa/destroy/%7bid%7d)

*Parâmetros*
- _token (string | obrigatório)

## Inserir endereço
**Método:** `POST`
[http://127.0.0.1:8000/api/endereco/store/{pessoa_id}](http://127.0.0.1:8000/api/endereco/store/%7bpessoa_id%7d)

*Parâmetros*
- cep (string | obrigatório)
- endereco (string | obrigatório)
- cidade (string | obrigatório)
- estado (string | obrigatório) – Apenas UFs Brasileiras
- numero (string | obrigatório)
- complemento (string | Não obrigatório)
- titulo (string | obrigatório)
- _token (string | obrigatório)

## Visualizar endereço
**Método:** `GET`
[http://127.0.0.1:8000/api/endereco/view/{id}](http://127.0.0.1:8000/api/endereco/view/%7bid%7d)

## Atualizar endereço
**Método:** `PUT`
[http://127.0.0.1:8000/api/endereco/update/{id}](http://127.0.0.1:8000/api/endereco/update/%7bid%7d)

*Parâmetros*
- cep (string | Não obrigatório)
- endereco (string | Não obrigatório)
- cidade (string | Não obrigatório)
- estado (string | Não obrigatório) – Apenas UFs Brasileiras
- numero (string | Não obrigatório)
- complemento (string | Não obrigatório)
- titulo (string | Não obrigatório)
- _token (string | obrigatório)

## Deletar endereço
**Método:** `DELETE`
[http://127.0.0.1:8000/api/endereco/destroy/{id}](http://127.0.0.1:8000/api/endereco/destroy/%7bid%7d)

*Parâmetros*
- _token (string | obrigatório)

 # Testes Automatizados

 Para teste automatizado estou utilizando o Laravel Dusk ([https://laravel.com/docs/5.7/dusk](https://laravel.com/docs/5.7/dusk)), a escolha pelo modelo deve-se ao fato de realizar testes de Browser e funcionais além da possibilidade de aceitar os comandos do phpunit, os arquivos de testes codificados estão disponíveis no caminho: [https://gitlab.com/roberson.pinheiro/selecao-2019/tree/master/tests/Browser](https://gitlab.com/roberson.pinheiro/selecao-2019/tree/master/tests/Browser).

Comando para rodar o teste:
> php artisan dusk

 **Atenção:** Em ambiente windows, talvez seja necessário executar o arquivo chromedrive.exe disponível no endereço [https://chromedriver.storage.googleapis.com/73.0.3683.20/chromedriver_win32.zip](https://chromedriver.storage.googleapis.com/73.0.3683.20/chromedriver_win32.zip) para utilização do Laravel Dusk.

# Informações complementares

Por padrão a Framework Laravel utiliza nos retornos de exceções páginas HTML, porém como se trata de uma API, foi modificado a função padrão de retorno para que o resultado fosse no formato JSON.

**Exceptions retorno JSON** 
**Arquivo:** app\Exceptions\Handler.php
**Função:** `render`

    public function render($request, Exception $exception)
    {
    	$retorno = [
    		'status' => false,
    		'error' => [
    			'code' => $exception->getCode(),
    			'message' => env('APP_DEBUG') ? $exception->getMessage() : '',
    		],
    	];
    	return response()->json($retorno);
    }

**Providers/Validação**

No projeto foi utilizado as validações padrão da Laravel, como exemplo foi criado uma provider que apenas verifica se a UF digitada no cadastro de endereço existe.

**Caminho:** `app\Providers\EstadoServiceProvider.php`