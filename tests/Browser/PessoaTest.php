<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Pessoa;

class PessoaTest extends DuskTestCase
{
    /**
     * A basic browser test example.
     *
     * @return void
     */
    public function testPessoaStore()
    {
        \Session::start();
        $response = $this->withHeaders(['X-Header' => 'Value',
            ])->json('POST', route('pessoa.store'), [
                'nome' => 'Roberson Augusto Pinheiro',
                'data_nascimento' => '1986-08-11',
                'sexo' => '1',
                'email' => 'roberson.augusto@gmail.com',
                'telefone' => '',
                'telefone_celular' => '(61)98605-1978',
                '_token' => csrf_token(),
            ]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'status' => true,
            ]);
    }
    
    public function testPessoaList()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit(route('pessoa.pessoas'))
                    ->assertSee('"status":"true"');
        });
    }

    public function testPessoaView()
    {
        //pega o id do usuario cadastrado com o email roberson.augusto@gmail.com        

        $this->browse(function (Browser $browser) {
            $pessoa = Pessoa::where('email', 'roberson.augusto@gmail.com')->first();
            $browser->visit(route('pessoa.view',['id' => $pessoa->id]))
                    ->assertSee('"status":"true"');
        });
    }

    public function testPessoaUpdate()
    {
        \Session::start();

        $pessoa = Pessoa::where('email', 'roberson.augusto@gmail.com')->first();

        $response = $this->withHeaders(['X-Header' => 'Value',
            ])->json('PUT', route('pessoa.update', ['id' => $pessoa->id]), [
                'nome' => 'Roberson Augusto Pinheiros2',
                '_token' => csrf_token(),
            ]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'status' => true,
            ]);
    }

    public function testPessoaDelete()
    {
        \Session::start();

        $pessoa = Pessoa::where('email', 'roberson.augusto@gmail.com')->first();

        $response = $this->withHeaders(['X-Header' => 'Value',
            ])->json('DELETE', route('pessoa.destroy', ['id' => $pessoa->id]), [
                '_token' => csrf_token(),
            ]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'status' => true,
            ]);
    }
}