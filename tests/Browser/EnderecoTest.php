<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Pessoa;
use App\Endereco;

class EnderecoTest extends DuskTestCase
{
    /**
     * A basic browser test example.
     *
     * @return void
     */

    //Cria pessoa para poder criar o endereco no teste
    public function testEnderecoPessoaStore()
    {
        \Session::start();
        $response = $this->withHeaders(['X-Header' => 'Value',
            ])->json('POST', route('pessoa.store'), [
                'nome' => 'Roberson Augusto Pinheiro',
                'data_nascimento' => '1986-08-11',
                'sexo' => '1',
                'email' => 'roberson.augusto@gmail.com',
                'telefone' => '',
                'telefone_celular' => '(61)98605-1978',
                '_token' => csrf_token(),
            ]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'status' => true,
            ]);
    }

    public function testEnderecoStore()
    {
        \Session::start();
        $pessoa = Pessoa::where('email', 'roberson.augusto@gmail.com')->first();
        $response = $this->withHeaders(['X-Header' => 'Value',
            ])->json('POST', route('endereco.store',['pessoasId' => $pessoa->id]), [
                'cep' => '72445-020',
                'endereco' => 'QI 02 lote 220',
                'cidade' => 'Gama',
                'estado' => 'DF',
                'numero' => '501',
                'titulo' => 'Casa 3',
                'pessoa_id' => $pessoa->id,
                '_token' => csrf_token(),
            ]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'status' => true,
            ]);
    }

    public function testEnderecoView()
    {
        //pega o id do usuario cadastrado com o email roberson.augusto@gmail.com
        $this->browse(function (Browser $browser) {
            $pessoa = Pessoa::where('email', 'roberson.augusto@gmail.com')->first();
            $endereco = Endereco::where('pessoa_id', $pessoa->id)->orderBy('id', 'desc')->first();

            $browser->visit(route('endereco.view',['id' => $endereco->id]))
                    ->assertSee('"status":"true"');
        });
    }

    public function testEnderecoUpdate()
    {
        \Session::start();

        $pessoa = Pessoa::where('email', 'roberson.augusto@gmail.com')->first();
        $endereco = Endereco::where('pessoa_id', $pessoa->id)->orderBy('id', 'desc')->first();

        $response = $this->withHeaders(['X-Header' => 'Value',
            ])->json('PUT', route('pessoa.update', ['id' => $endereco->id]), [
                'titulo' => 'Casa 1',
                '_token' => csrf_token(),
            ]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'status' => true,
            ]);
    }

    public function testEnderecoDelete()
    {
        \Session::start();

        $pessoa = Pessoa::where('email', 'roberson.augusto@gmail.com')->first();
        $endereco = Endereco::where('pessoa_id', $pessoa->id)->orderBy('id', 'desc')->first();

        $response = $this->withHeaders(['X-Header' => 'Value',
            ])->json('DELETE', route('endereco.destroy', ['id' => $endereco->id]), [
                '_token' => csrf_token(),
            ]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'status' => true,
            ]);
    }

    public function testEnderecoPessoaDelete()
    {
        \Session::start();

        $pessoa = Pessoa::where('email', 'roberson.augusto@gmail.com')->first();

        $response = $this->withHeaders(['X-Header' => 'Value',
            ])->json('DELETE', route('pessoa.destroy', ['id' => $pessoa->id]), [
                '_token' => csrf_token(),
            ]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'status' => true,
            ]);
    }
}